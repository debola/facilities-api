﻿namespace Monitor.WF
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Do_Work = new System.Windows.Forms.Button();
            this.settings = new System.Windows.Forms.Button();
            this.spinner_img = new System.Windows.Forms.PictureBox();
            this.indicator_btn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.spinner_img)).BeginInit();
            this.SuspendLayout();
            // 
            // Do_Work
            // 
            this.Do_Work.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Do_Work.Location = new System.Drawing.Point(247, 151);
            this.Do_Work.Name = "Do_Work";
            this.Do_Work.Size = new System.Drawing.Size(262, 90);
            this.Do_Work.TabIndex = 0;
            this.Do_Work.Text = "Sync";
            this.Do_Work.UseVisualStyleBackColor = false;
            this.Do_Work.Click += new System.EventHandler(this.Do_Work_Click);
            // 
            // settings
            // 
            this.settings.Location = new System.Drawing.Point(706, 12);
            this.settings.Name = "settings";
            this.settings.Size = new System.Drawing.Size(82, 41);
            this.settings.TabIndex = 1;
            this.settings.Text = "Settings";
            this.settings.UseVisualStyleBackColor = true;
            this.settings.Click += new System.EventHandler(this.settings_Click);
            // 
            // spinner_img
            // 
            this.spinner_img.Image = ((System.Drawing.Image)(resources.GetObject("spinner_img.Image")));
            this.spinner_img.Location = new System.Drawing.Point(227, 103);
            this.spinner_img.Name = "spinner_img";
            this.spinner_img.Size = new System.Drawing.Size(282, 200);
            this.spinner_img.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.spinner_img.TabIndex = 2;
            this.spinner_img.TabStop = false;
            this.spinner_img.Visible = false;
            // 
            // indicator_btn
            // 
            this.indicator_btn.Location = new System.Drawing.Point(12, 18);
            this.indicator_btn.Name = "indicator_btn";
            this.indicator_btn.Size = new System.Drawing.Size(135, 29);
            this.indicator_btn.TabIndex = 3;
            this.indicator_btn.Text = "Post Indicator";
            this.indicator_btn.UseVisualStyleBackColor = true;
            this.indicator_btn.Click += new System.EventHandler(this.indicator_btn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.indicator_btn);
            this.Controls.Add(this.spinner_img);
            this.Controls.Add(this.settings);
            this.Controls.Add(this.Do_Work);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monitor";
            ((System.ComponentModel.ISupportInitialize)(this.spinner_img)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Button Do_Work;
        private Button settings;
        private PictureBox spinner_img;
        private Button indicator_btn;
    }
}