﻿using System.ComponentModel.DataAnnotations;

namespace Monitor.Model
{
    public enum FacilityState : byte
    {
        [Display(Name = "Benue")]Benue = 1,
        [Display(Name = "Lagos")] Lagos,
        [Display(Name = "Nasarawa")] Nasarawa,
        [Display(Name = "FCT")] FCT,
        [Display(Name = "Delta")] Delta,
        [Display(Name = "Enugu")] Enugu,
        [Display(Name = "Kaduna")] Kaduna,
        [Display(Name = "Rivers")] Rivers,
        [Display(Name = "Plateau")] Plateau
    }

    public enum Gender : byte
    {
        [Display(Name = "M")]
        M,
        [Display(Name = "F")]
        F
    }

    public enum FormType : int
    {
        None = 0,
        [Display(Name = "TB Screening and Case Indetification Forms")]
        TB_Screening_and_Case_Indetification_Forms,
        [Display(Name = "TB Laboratory Register")]
        TB_Laboratory_Register,
        [Display(Name = "TB Treatment")]
        TB_Treatment,
        [Display(Name = "Pharmacy Order Form")]
        PhamarcyOrderForm
    }

    public enum ComplaintType : byte
    {
        Others = 0,
        [Display(Name = "Data loss")]
        Data_Loss,
        [Display(Name = "Poor availability")]
        Poor_Availability,
        [Display(Name = "No required field")]
        No_required_field,
        [Display(Name = "Error on form submission")]
        Error_on_Form_Submission,
        [Display(Name = "Users name creation error")]
        Users_Name_Creation_Error,
        [Display(Name = "No valid option")]
        No_Valid_Option,
    }
}