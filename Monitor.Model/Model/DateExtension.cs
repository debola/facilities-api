﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor.Model
{
    public static class DateExtension
    {
        public static DateTime StartOfDay(this DateTime timestamp) =>
            timestamp.Subtract(timestamp.TimeOfDay);

        public static DateTime EndOfDay(this DateTime timestamp) =>
            timestamp.StartOfDay().AddDays(1).AddMilliseconds(-1);
    }
}
