﻿using Facilities.API.App;
using Facilities.API.App.Config;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Monitor.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Facilities.API.Services
{
    public interface IFacilityRepository
    {
        Task<DashboardDataInfo> GetDashboardDataInfoAsync(FacilityFilterDTO filter);
        Task<bool> SaveFacilityDataAsync(FacilityData[] facilityData);
        Task<FormSubmission[]> GetFormSubmissionsPerState(int formId, string state);
        Task<IEnumerable<TBIndicatorInfo>> GetTBIndicators();
        Task<bool> PostTBIndicatorAsync(TBIndicatorInfo[] tBIndicators);
        Task<IEnumerable<TBIndicatorInfo>> GetTBIndicators_Mock_Data();
        Task<IEnumerable<DataAbstractorInfoDistribution>> GetDataAbstractorInfo(string datimCode);
        Task<IEnumerable<FacilityDTO>> GetStateFacilities(string state);
        Task<bool> ReportComplaint(ComplaintDTO complaint);
        Task<IEnumerable<ComplaintResult>> GetReports();
    }

    public class FacilityRepository : IFacilityRepository
    {
        private readonly FacilityDbContext dbContext;
        private readonly ILogger<FacilityRepository> logger;
        private readonly IOptions<MonitorConfig> monitorConfig;

        /// <summary>
        /// Form types to check from
        /// </summary>
        private readonly int[] TB_Screening_and_Case_Indetification_Forms = new int[] { 98, 92, 85, 97, 199 };
        private readonly int[] TB_Laboratory_Register = new int[] { 102, 89, 99 };
        private readonly int[] TB_Treatment = new int[] { 82, 100, 101, 94, 93, 88, 81 };
        public FacilityRepository(FacilityDbContext dbContext, ILogger<FacilityRepository> logger, IOptions<MonitorConfig> monitorConfig)
        {
            this.dbContext = dbContext;
            this.logger = logger;
            this.monitorConfig = monitorConfig;
        }


        #region Complaint
        public async Task<bool> ReportComplaint(ComplaintDTO complaint)
        {
            //var facility = await dbContext.Facility.FirstOrDefaultAsync(x => x.DatimCode.ToLower() == complaint.FacilityName.ToLower());
            var facilityState = await GetFacility(complaint.State);
            var facility = facilityState.FirstOrDefault(x => x.DatimCode.ToLower() == complaint.FacilityName.ToLower());

            if (facility == null)
                return false;

            var report = new Complaints
            {
                DateCreated = DateTimeOffset.Now,
                Name = complaint.Name,
                FacilityName = facility.FacilityName,
                State = complaint.State,
                Message = complaint.ComplaintMessage,
                Complaint = complaint.Complaint

            };

            await dbContext.Complaints.AddAsync(report);
            return await dbContext.SaveChangesAsync() > 0;
        }

        public async Task<IEnumerable<ComplaintResult>> GetReports()
        {
            try
            {
                return await dbContext.Complaints.Select(complaint => new ComplaintResult
                {
                    Entered = complaint.DateCreated.DateTime.ToString("dd/MMM/yyyy"),
                    Name = complaint.Name,
                    FacilityName = complaint.FacilityName,
                    State = complaint.State,
                    ComplaintMessage = complaint.Message,
                    Complaint = complaint.Complaint //get the right enum name
                }).ToListAsync();
            }
            catch (Exception)
            {
                return Enumerable.Empty<ComplaintResult>();
            }
            
        }
        #endregion

        public async Task<IEnumerable<DataAbstractorInfoDistribution>> GetDataAbstractorInfo(string datimCode)
        {
            var facilities = await GetFacilityForm(datimCode);
            //var facilities = this.dbContext.Facility.Where(f => f.DatimCode.ToLower() == datim.ToLower()); //new state string will be created here, fix it
            return facilities.GroupBy(x => x.Username).Select(x => new DataAbstractorInfoDistribution
            {
                FacilityName = x.Select(x => x.FacilityName).FirstOrDefault(),
                AbstractorName = x.Key,
                TB_Screening_Count = x.Count(x => x.FormId == 98),
                TB_ReferralForm_Count = x.Count(x => x.FormId == 92),
                TB_Index_Patient_Ivestigation_Count = x.Count(x => x.FormId == 85),
                Presumptive_TB_Register_Count = x.Count(x => x.FormId == 97),
                TB_Laboratory_Form_Count = x.Count(x => x.FormId == 102),
                Specimen_Request_Form_Count = x.Count(x => x.FormId == 89),
                Specimen_Result_Form_Count = x.Count(x => x.FormId == 99),
                DR_TB_Treartment_Register_Count = x.Count(x => x.FormId == 82),
                TB_Patient_Referral_Transfer_Count = x.Count(x => x.FormId == 100),
                TB_treatment_initiation_card_Count = x.Count(x => x.FormId == 101),
                TB_treament_monitoring_form_Count = x.Count(x => x.FormId == 94),
                TB_interruption_tracking_form_Count = x.Count(x => x.FormId == 93),
                DR_TBInPatient_Discharge_Form_Count = x.Count(x => x.FormId == 88),
                TB_LGA_Health_Facility_Register_Count = x.Count(x => x.FormId == 81),
            });
        }
        #region Forms
        public async Task<FormSubmission[]> GetFormSubmissionsPerState(int formId, string state)
        {
            try
            {
                var startDate = new DateTime(year: 2019, 01, 01).StartOfDay();
                var endDate = DateTime.Now.EndOfDay(); //dates should be service for easy testing 
                var query1 = await GetFacility(state, startDate, endDate);
                var query = query1.Where(x => x.FormId == formId);
                //var query = dbContext.Facility.Where(x => x.State.ToLower() == state.ToLower() && x.FormId == formId);
                if (!query.Any())
                    return Array.Empty<FormSubmission>();

                var formSubmission = query.GroupBy(x => x.DatimCode).Select(x => new FormSubmission
                {
                    FormName = x.Select(f => f.FormName).First(),
                    FacilityName = x.Select(f => f.FacilityName).FirstOrDefault(),
                    Submissions = x.Sum(f => f.Submissions)
                }).ToArray();

                return formSubmission;
            }
            catch (Exception ex)
            {
#pragma warning disable CA2254 // Template should be a static expression
                logger.LogError($"There was an issue getting data: {ex.Message}");
#pragma warning restore CA2254 // Template should be a static expression
                return Array.Empty<FormSubmission>();
            }

        }
        private (int Id, FormType type) GetFormInfo(string name)
        {
            var form = monitorConfig.Value.Forms.FirstOrDefault(f => f.Name.ToLower() == name.ToLower());

            if (form == null)
                throw new ArgumentException("Form name does not exist in our record");

            if (TB_Screening_and_Case_Indetification_Forms.Contains(form.Id))
                return (Id: form.Id, type: FormType.TB_Screening_and_Case_Indetification_Forms);
            if (TB_Treatment.Contains(form.Id))
                return (Id: form.Id, type: FormType.TB_Treatment);
            if (TB_Laboratory_Register.Contains(form.Id))
                return (Id: form.Id, type: FormType.TB_Laboratory_Register);

            throw new ArgumentException("Form name does not exist in our record");
        }

        #endregion

        #region Facility
        private async Task SaveFacilityData(FacilityData facilityData)
        {
            var (formId, formType) = GetFormInfo(facilityData.Name);
            var date = DateTime.TryParse(facilityData.DateCreated, out DateTime dateCreated);

            var facility = new Facility
            {
                FacilityName = facilityData.FacilityName,
                DatimCode = facilityData.DatimCode,
                FacilityId = facilityData.FacilityId,
                ImplementingPartner = facilityData.ImplementingPartner,
                LGA = facilityData.LGA,
                State = facilityData.State,
                Submissions = facilityData.Submissions,
                FormId = formId,
                FormType = formType,
                FormName = facilityData.Name,
                DateCreated = date ? dateCreated : DateTime.MinValue,
                PatientId = facilityData.PatientId,
                Sex = GetGender(facilityData.Sex.ToUpper()),
                CurrentAge = facilityData.CurrentAge,
                Username = facilityData.Username,

            };
            var dateNow = DateTime.Now;
            var dataExistsAlready = dbContext.Facility.Any(
                f => f.DatimCode == facility.DatimCode
                && f.PatientId == facility.PatientId
                && f.FormId == formId
                && (f.DateCreated.Month == facility.DateCreated.Month && f.DateCreated.Year == facility.DateCreated.Year));

            if (dataExistsAlready)
                return;

            await dbContext.Facility.AddAsync(facility);

            await dbContext.SaveChangesAsync();
        }
        private async Task<FacilityInfo[]> GetFacilities(FacilityFilterDTO filter)
        {
            var state = filter.State;
            var startDate = filter.StartDate ?? new DateTime(year: 2019, 01, 01);
            var endDate = filter.EndDate ?? DateTime.Now; //dates should be service for easy testing 
            try
            {
                var facilities = await GetFacility(state, startDate.StartOfDay(), endDate.EndOfDay());
                //var facilities = this.dbContext.Facility.Where(f => f.State.ToLower() == state.ToLower() && f.DateCreated >= startDate && f.DateCreated <= endDate); //new state string will be created here, fix it
                var facilityGroup = facilities.GroupBy(x => new
                {
                    x.DatimCode
                }).Select(x => new FacilityInfo
                {
                    DatimCode = x.Select(x => x.DatimCode).First(),
                    Name = x.Select(x => x.FacilityName).First(),
                    State = x.Select(x => x.State).First(),
                    Submissions = x.DistinctBy(x => x.PatientId).Count(),

                    Patients = x.Select(x => new PatientInfo
                    {
                        Age = x.CurrentAge,
                        PatientId = x.PatientId,
                        Gender = x.Sex
                    }).ToArray(),

                    DataAbstractors = x.Select(x => new DataAbstractor
                    {
                        FacilityName = x.FacilityName,
                        AbstractorName = x.Username,
                        SubmissionCount = facilities.Where(c => c.Username == x.Username).Sum(x => x.Submissions)
                    }).ToArray(),
                }).ToArray();

                return facilityGroup;
            }
            catch (Exception ex)
            {
#pragma warning disable CA2254 // Template should be a static expression
                logger.LogError($"There was an issue getting data: {ex.Message}");
#pragma warning restore CA2254 // Template should be a static expression
                throw;
            }

        }
        private async Task<Facility[]> GetFacility(string state, DateTime? startDate = null, DateTime? endDate = null)
        {
            var path = Path.Combine("wwwroot", "data", "facilityData.json");
            string jsonContent = await System.IO.File.ReadAllTextAsync(path);

            var facilityData = JsonSerializer.Deserialize<Facility[]>(jsonContent);
            var fac = facilityData.Where(f => f.State.ToLower() == state.ToLower()).ToArray();
            if (startDate.HasValue && endDate.HasValue)
                fac = fac.Where(f => f.DateCreated >= startDate && f.DateCreated <= endDate).ToArray();
            return fac;
        }

        private async Task<Facility[]> GetFacilityForm(string datimCode)
        {
            var path = Path.Combine("wwwroot", "data", "facilityData.json");
            string jsonContent = await System.IO.File.ReadAllTextAsync(path);

            var facilityData = JsonSerializer.Deserialize<Facility[]>(jsonContent);
            return facilityData.Where(f => f.DatimCode.ToLower() == datimCode.ToLower()).ToArray();
        }

        public async Task<DashboardDataInfo> GetDashboardDataInfoAsync(FacilityFilterDTO filter)
        {
            var query = await GetFacilities(filter);
            if (!query.Any())
                return null;

            else
            {
                return new DashboardDataInfo()
                {
                    SubmissionDistribution = new SubmissionDistribution
                    {
                        FacilitySubmissions = query.Select(x => new FacilitySubmission
                        {
                            FacilityName = x.Name,
                            Submissions = x.Submissions
                        })
                    },
                    PatientDistribution = query.Select(x => new PatientDistribution
                    {
                        FacilityName = x.Name,
                        MaleCount = x.Patients.Count(x => x.Gender == Gender.M),
                        FemaleCount = x.Patients.Count(x => x.Gender == Gender.F),
                        TotalPatientCount = x.Patients.Length
                    }),
                    AgeDistribution = AgeDistribution(query.SelectMany(x => x.Patients)),
                    DataAbstractorsFacilityCountDistribution = query.Select(a => new DataAbstractorDistribution
                    {
                        FacilityName = a.Name,
                        DatimCode = a.DatimCode,
                        AbstractorCount = a.DataAbstractors.Length
                    }),
                    //DataAbstractorsInfoDistribution = query.Select(a => new DataAbstractorInfoDistribution
                    //{
                    //    FacilityName = a.Name,
                    //    AbstractorName = dataAbstractorInfo.FirstOrDefault(x => x.facilityName == a.Name).AbstractorName,
                    //    SubmissionCount = dataAbstractorInfo.FirstOrDefault(x => x.facilityName == a.Name).SubmissionCount,
                    //})
                };

            }

        }
        private IEnumerable<AgeDistribution> AgeDistribution(IEnumerable<PatientInfo> patients)
        {
            return patients
                .GroupBy(p => new { Age = 5 * (p.Age / 5), Gender = p.Gender })
                .OrderBy(x => x.Key.Age)
                .Select(s => new AgeDistribution
                {
                    Range = $"{s.Key.Age}-{s.Key.Age + 4}",
                    MaleCount = s.Count(x => x.Gender == Gender.M),
                    FemaleCount = s.Count(x => x.Gender == Gender.M)
                }).ToList();
        }


        public async Task<bool> SaveFacilityDataAsync(FacilityData[] facilityData)
        {
            try
            {
                foreach (var item in facilityData)
                {
                    await SaveFacilityData(item);
                }
                return true;
            }
            catch (Exception ex)
            {
#pragma warning disable CA2254 // Template should be a static expression
                logger.LogError($"There was an issue saving data: {ex.Message}");
#pragma warning restore CA2254 // Template should be a static expression
                throw;
            }
        }
        #endregion

        private Gender GetGender(string gender) => gender.ToUpper() == "F" ? Gender.F : Gender.M;

        public async Task<IEnumerable<FacilityDTO>> GetStateFacilities(string state)
        {
            //var facility = await dbContext.Facility.Where(s => s.State.ToLower() == state.ToLower()).Select(x => new FacilityDTO
            //{
            //    DatimCode = x.DatimCode,
            //    Name = x.FacilityName
            //}).ToListAsync();

            var facility = await GetFacility(state);

            var facilities = facility.GroupBy(x => x.DatimCode).Select(x => new FacilityDTO
            {
                DatimCode = x.Key,
                Name = x.Select(x => x.FacilityName).First()
            });

            return facilities;
        }


        #region DataAbstractor
        private async Task<DataAbstractorData[]> GetDataAbstractor(string state) //should reafctor but no time
        {
            var path = Path.Combine("wwwroot", "data", "dataAbstractors.json");
            string jsonContent = await System.IO.File.ReadAllTextAsync(path);

            var facilityData = JsonSerializer.Deserialize<DataAbstractorData[]>(jsonContent);
            return facilityData.Where(f => f.State.ToLower() == state.ToLower()).ToArray();
        }


        public class DataAbstractorData
        {
            public string State { get; set; }
            public string FacilityName { get; set; }
            public int DataAbstractor { get; set; }
        }
        #endregion

        #region TbIndicators
        public async Task<bool> PostTBIndicatorAsync(TBIndicatorInfo[] tBIndicators)
        {
            try
            {
                foreach (var tBIndicator in tBIndicators)
                {
                    bool isExist = await dbContext.TBIndicator.AnyAsync(x => x.DatimCode == tBIndicator.Datim_Code && x.ART_Number == tBIndicator.ART_Number);
                    if (isExist)
                       continue;

                    await SaveTBIndicators(tBIndicator);
                }
                return true;
            }
            catch (Exception ex)
            {
                #pragma warning disable CA2254 // Template should be a static expression
                logger.LogError($"There was an issue saving data: {ex.Message}");
                #pragma warning restore CA2254 // Template should be a static expression
                throw;
            }
        }

        public async Task<bool> SaveTBIndicators(TBIndicatorInfo tBIndicator)
        {
            try
            {
                var dbIndicator = new TBIndicator
                {
                    DatimCode = tBIndicator.Datim_Code,
                    ART_Number = tBIndicator.ART_Number,
                    IP = tBIndicator.IP,
                    State = tBIndicator.State,
                    Facility_Name = tBIndicator.Facility_Name,
                    Sex = GetGender(tBIndicator.Sex),
                    DOB = tBIndicator.DOB,
                    Enrolment_Date = tBIndicator.Enrolment_Date,
                    Weight_At_Enrolment = tBIndicator.Weight_At_Enrolment,
                    WHO_Stage_AtEnrolment = tBIndicator.WHO_Stage_AtEnrolment,
                    Symptoms = tBIndicator.Symptoms,
                    CD4_Count_At_HIV_Diagnosis = tBIndicator.CD4_Count_At_HIV_Diagnosis,
                    Date_Of_First_CD4 = tBIndicator.Date_Of_First_CD4,
                    TB_Status_At_Enrolment = tBIndicator.TB_Status_At_Enrolment,
                    RegimenAtARTStart = tBIndicator.RegimenAtARTStart,
                    Prior_ART = tBIndicator.Prior_ART,
                    ART_Start_Date = tBIndicator.ART_Start_Date,
                    Current_ARV_Regimen = tBIndicator.Current_ARV_Regimen,
                    Reasons_For_Stopping_ARVs = tBIndicator.Reasons_For_Stopping_ARVs,
                    Current_Weight = tBIndicator.Current_Weight,
                    Current_Weight_Date = tBIndicator.Current_Weight_Date,
                    TPT_Start_Date = tBIndicator.TPT_Start_Date,
                    TPT_Completion_Date = tBIndicator.TPT_Completion_Date,
                    Current_TB_Status = tBIndicator.Current_TB_Staatus,
                    CD4_Count_Current = tBIndicator.CD4_Count_Current,
                    Date_Of_Current_CD4 = tBIndicator.Date_Of_Current_CD4,
                    CurrentViralLoad = tBIndicator.CurrentViralLoad,
                    DateofCurrentViralLoad = tBIndicator.DateofCurrentViralLoad,
                    TB_Treatment_Completed = tBIndicator.TB_Treatment_Completed,
                };

                var existingData = dbContext.TBIndicator.FirstOrDefault(x => x.DatimCode == tBIndicator.Datim_Code && x.ART_Number == tBIndicator.ART_Number);

                if (existingData != null)
                    dbContext.TBIndicator.Remove(existingData);
                await dbContext.AddAsync(dbIndicator);
                return await dbContext.SaveChangesAsync() > 0;
            }
            catch (Exception ex)
            {
#pragma warning disable CA2254 // Template should be a static expression
                logger.LogError($"There was an issue saving data: {ex.Message}");
#pragma warning restore CA2254 // Template should be a static expression
                throw;
            }
            
        }

        public async Task<IEnumerable<TBIndicatorInfo>> GetTBIndicators()
        {
            var dbIndicators = await dbContext.Set<TBIndicator>().ToListAsync();

            if (dbIndicators.Count < 1)
                return Array.Empty<TBIndicatorInfo>();

            var tBIndicatorInfos = new List<TBIndicatorInfo>();
            foreach (var item in dbIndicators)
            {
                var tbIndicatorInfo = new TBIndicatorInfo
                {
                    Datim_Code = item.DatimCode,
                    ART_Number = item.ART_Number,
                    IP = item.IP,
                    State = item.State,
                    Facility_Name = item.Facility_Name,
                    Sex = item.Sex.ToString(),
                    DOB = item.DOB,
                    Enrolment_Date = item.Enrolment_Date,
                    Weight_At_Enrolment = item.Weight_At_Enrolment,
                    WHO_Stage_AtEnrolment = item.WHO_Stage_AtEnrolment,
                    Symptoms = item.Symptoms,
                    CD4_Count_At_HIV_Diagnosis = item.CD4_Count_At_HIV_Diagnosis,
                    Date_Of_First_CD4 = item.Date_Of_First_CD4,
                    TB_Status_At_Enrolment = item.TB_Status_At_Enrolment,
                    RegimenAtARTStart = item.RegimenAtARTStart,
                    Prior_ART = item.Prior_ART,
                    ART_Start_Date = item.ART_Start_Date,
                    Current_ARV_Regimen = item.Current_ARV_Regimen,
                    Reasons_For_Stopping_ARVs = item.Reasons_For_Stopping_ARVs,
                    Current_Weight = item.Current_Weight,
                    Current_Weight_Date = item.Current_Weight_Date,
                    TPT_Start_Date = item.TPT_Start_Date,
                    TPT_Completion_Date = item.TPT_Completion_Date,
                    Current_TB_Staatus = item.Current_TB_Status,
                    CD4_Count_Current = item.CD4_Count_Current,
                    Date_Of_Current_CD4 = item.Date_Of_Current_CD4,
                    CurrentViralLoad = item.CurrentViralLoad,
                    DateofCurrentViralLoad = item.DateofCurrentViralLoad,
                    TB_Treatment_Completed = item.TB_Treatment_Completed,
                };
                tBIndicatorInfos.Add(tbIndicatorInfo);
            }
            return tBIndicatorInfos;
        }

        public async Task<IEnumerable<TBIndicatorInfo>> GetTBIndicators_Mock_Data()
        {
            var path = Path.Combine("wwwroot", "data", "tb_indicator_data.json");
            string jsonContent = await System.IO.File.ReadAllTextAsync(path);

            var indicators = JsonSerializer.Deserialize<IEnumerable<TBIndicatorInfo>>(jsonContent);
            return indicators;
        }
        #endregion

    }
}
