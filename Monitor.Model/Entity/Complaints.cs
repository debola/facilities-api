﻿namespace Monitor.Model
{
    public class Complaints
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string FacilityName { get; set; }
        public string Complaint { get; set; }
        public string Message { get; set; }
        public DateTimeOffset DateCreated { get; set; }
    }
}
