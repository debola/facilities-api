﻿using Facilities.API.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Monitor.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Facilities.API.Controllers
{
    [ApiController]
    [Route("/api/[controller]")]
    public class FacilitiesController : ControllerBase
    {
        private readonly ILogger<FacilitiesController> _logger;
        private readonly IFacilityRepository facility;

        public FacilitiesController(ILogger<FacilitiesController> logger, IFacilityRepository facility)
        {
            _logger = logger;
            this.facility = facility;
        }

        [HttpGet("GetDashboardData")]
        public async Task<IActionResult> GetDashboardData([FromQuery] FacilityFilterDTO filter)
        {
            if (!ModelState.IsValid)
                return BadRequest("please send in the right data");
            try
            {
                var facilityData = await this.facility.GetDashboardDataInfoAsync(filter);

                if (facilityData == null)
                    return new NoContentResult();

                return new JsonResult(facilityData);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was an error getting the facility data");
            }
            
        }

        [HttpGet]
        public async Task<IActionResult> Get(string state)
        {
            if (string.IsNullOrWhiteSpace(state))
                return BadRequest("please send in the right data");

            var facilities = await this.facility.GetStateFacilities(state);
            return Ok(facilities);
        }

        [HttpGet("GetForms")]
        public async Task<IActionResult> Get([FromQuery] FormDTO form)
        {
            if (!ModelState.IsValid)
                return BadRequest("please send in the right data");
            try
            {
                var formSubmission = await this.facility.GetFormSubmissionsPerState(form.FormId, form.State);

                if (formSubmission.Length < 1)
                    return new NoContentResult();

                return new JsonResult(formSubmission);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was an error getting the form data");
            }

        }

        /// <summary>
        /// Endpoint that gets the facilities data and sends to another server <br/>
        /// The endpoint will be like <code>https://your-domain-dot-com/api/Facilities/UpdateFacilities</code>
        /// </summary>
        /// <param name="facilityInfo"></param>
        /// <returns><see cref="StatusCodeResult"/> depending on the success or failure of this action method </returns>
        [HttpPost("UpdateFacilities")]
        public async Task<IActionResult> UpdateFacilities(FacilityData[] facilityInfo)
        {
            if (!ModelState.IsValid)
                return BadRequest("please send in the right data");

            if (!facilityInfo.Any())
                return StatusCode(StatusCodes.Status204NoContent);
            try
            {
                var isSaved = await this.facility.SaveFacilityDataAsync(facilityInfo);

                return isSaved ?
                        StatusCode(StatusCodes.Status201Created, "Request successful") :
                    StatusCode(StatusCodes.Status500InternalServerError, "Request unsuccessful. There was an error posting your data");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Request unsuccessful. There was an error posting your data: {ex.Message}");
            }
        }

        [HttpPost("PostTbIndicators")]
        public async Task<IActionResult> PostToTable(TBIndicatorInfo[] TBIndicator)
        {
            if (!ModelState.IsValid)
                return BadRequest("please send in the right data");

            if (!TBIndicator.Any())
                return StatusCode(StatusCodes.Status204NoContent);
            try
            {
                var result = await this.facility.PostTBIndicatorAsync(TBIndicator);
                return result ?
                        StatusCode(StatusCodes.Status201Created, "Request successful") :
                        StatusCode(StatusCodes.Status500InternalServerError, "Request unsuccessful. There was an error posting your data");
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Request unsuccessful. There was an error posting your data: {ex.Message}");
            }
            
        }

        [HttpGet("GetTbIndicators")]
        public async Task<IActionResult> GetTbIndicators()
        {
            try
            {
                //var result = await this.facility.GetTBIndicators_Mock_Data();
                var result = await this.facility.GetTBIndicators();
                return result.Any() ?
                        new JsonResult(result) :
                        NoContent();
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, $"Request unsuccessful. There was an error posting your data: {ex.Message}");
            }
            
        }

        [HttpGet("GetDataAbstractorInfo")]
        public async Task<IActionResult> GetDataAbstractorInfo(string datimCode)
        {
            try
            {
                var result = await this.facility.GetDataAbstractorInfo(datimCode);
                return result.Any() ?
                        new JsonResult(result) :
                        NoContent();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, "There was an error getting the data");
            }

        }

        [HttpPost("ReportComplaint")]
        public async Task<IActionResult> Complaint(ComplaintDTO complaint)
        {
                bool result = await this.facility.ReportComplaint(complaint);
                return result ?
                        StatusCode(StatusCodes.Status201Created, "success") :
                        StatusCode(StatusCodes.Status500InternalServerError, "fail");
        }

        [HttpPost("Login")]
        public IActionResult Login(LoginDTO user)
        {
            //this is just a test for the main time
            var result = new LoginResult();

            if (!ModelState.IsValid)
                return BadRequest("please send in the right data");

            if(user.Username.Equals("SuperAdmin", StringComparison.OrdinalIgnoreCase) 
                && user.Password.Equals("SuperAdmin1234", StringComparison.OrdinalIgnoreCase))
            {
                result.IsSuccessful = true;
                result.Role = 1;
            }
            if (user.Username.Equals("Admin", StringComparison.OrdinalIgnoreCase)
                && user.Password.Equals("Admin1234", StringComparison.OrdinalIgnoreCase))
            {
                result.IsSuccessful = true;
                result.Role = 2;
            }
            return result.IsSuccessful ?
                    Ok(result) :
                    Ok(result);
        }


        [HttpGet("ReportLogs")]
        public async Task<IActionResult> GetReports()
        {
            try
            {
                IEnumerable<ComplaintResult> result = await facility.GetReports();

                return Ok(result);
            }
            catch (Exception)
            {
                return Ok(Enumerable.Empty<ComplaintResult>()); //throw better exception and handle it on the client
            }
            

        }
    }
}
