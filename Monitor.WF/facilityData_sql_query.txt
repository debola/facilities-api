SELECT
  'APIN' AS "Implementing_Partner",
  (SELECT
    global_property.property_value
  FROM
    global_property
  WHERE property = 'Facility_Name') AS "Facility_Name",
  (SELECT
    global_property.property_value
  FROM
    global_property
  WHERE property = 'facility_local_id') AS "Facility_Id",
  (SELECT
    global_property.property_value
  FROM
    global_property
  WHERE property = 'Facility_Datim_Code') AS "Datim_Code",
  (SELECT
    location.city_village
  FROM
    location
    JOIN global_property
  WHERE location.name = global_property.property_value
  LIMIT 1) AS LGA,
  (SELECT
    location.state_province
  FROM
    location
    JOIN global_property
  WHERE location.name = global_property.property_value
  LIMIT 1) AS "State",
  patient_id,
  p.gender AS Sex,
  FLOOR (
    DATEDIFF (CURDATE(), p.birthdate) / 365.25
  ) AS "Current_Age",
  DATE_FORMAT (enc.date_created, '%d-%b-%Y') AS date_created,
  form.name,
  COUNT(*) AS submissions,
  IF(u.Username IS NOT NULL,CONCAT('TPT', '_', u.Username),'') AS Username
FROM
  encounter enc
  JOIN person p
    ON enc.`patient_id` = p.`person_id`
  JOIN form
    ON enc.form_id = form.form_id
    JOIN users u ON u.creator=form.`creator`
WHERE form.form_id IN (98,92,85,97,102,89,99,82,100,101,94,93,88,81,27,98)
  AND enc.date_created BETWEEN '2021-01-01'
  AND '2021-07-31'
  AND p.`voided` = 0
GROUP BY patient_id,
  form.name,
  DATE_FORMAT (enc.date_created, '%d-%b-%Y')
ORDER BY enc.date_created;