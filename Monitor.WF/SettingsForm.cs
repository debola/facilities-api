﻿using Monitor.Model.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Monitor.WF
{
    public partial class SettingsForm : Form
    {
        private string? Server;
        private string? Username;
        private string? Password;
        private string? Database;
        private int Port;
        public SettingsForm()
        {
            InitializeComponent();
            PopulateFormOnLoad();
        }

        private void PopulateFormOnLoad()
        {
            var configSettings = Helper.GetConfigurationSettings();
            server_name.Text = configSettings?.Server;
            uid.Text = configSettings?.Username;
            pwrd.Text = configSettings?.Password;
            database.Text = configSettings?.Database;
            port_number.Value = (decimal)(configSettings?.Port);
        }

        private void server_name_TextChanged(object sender, EventArgs e)
        {
            Server = server_name.Text;
        }

        private void uid_TextChanged(object sender, EventArgs e)
        {
            Username = uid.Text;
        }

        private void pwrd_TextChanged(object sender, EventArgs e)
        {
            Password = pwrd.Text;
        }

        private void database_TextChanged(object sender, EventArgs e)
        {
            this.Database = database.Text;
        }


        private async void Button1_Click(object sender, EventArgs e)
        {

            DialogResult dialogResult = MessageBox.Show("Save Settings?", "Save", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                await SaveINConfigFile();

                Application.Exit();
            }
            else if (dialogResult == DialogResult.No)
            {
                //do nothing
                
            }

        }

        private void cancel_btn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        async Task SaveINConfigFile()
        {
            try
            {
                var settings = new ConfigSettings
                {
                    Server = Server,
                    Username = Username,
                    Password = Password,
                    Database = Database,
                    Port = Port,
                };

                string json = JsonSerializer.Serialize(settings);
                string filePath = $"{AppDomain.CurrentDomain.BaseDirectory}settings.json";
                File.SetAttributes(filePath, FileAttributes.Normal);
                await File.WriteAllTextAsync(filePath, json);
            }
            catch (Exception ex)
            {
                MessageBox.Show($"please restart this application.{ex.Message}");

            }

        }

        private void port_number_ValueChanged(object sender, EventArgs e)
        {
            Port = (int)port_number.Value;
        }
    }
}
