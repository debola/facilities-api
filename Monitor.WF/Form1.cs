using Monitor.Model;
using MySql.Data.MySqlClient;
using System.Configuration;
using System.Text;
using System.Text.Json;
using System.Timers;

namespace Monitor.WF
{
    public partial class Form1 : Form
    {
        public string? ConnectionString { get; set; }
        public bool IsConfigurationSet { get; set; }
        public string? APIBase { get; set; }
        System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
        
        public Form1()
        {
            InitializeComponent();
            
            
            var config = Helper.GetConfigurationSettings();
            ConnectionString = config.ToString();
            APIBase = "https://localhost:5001/api/Facilities";

            timer1.Interval = 500000;//5 seconds
            timer1.Tick += new System.EventHandler(timer1_Tick);
            //timer1.Start();

            IsConfigurationSet = !string.IsNullOrWhiteSpace(ConnectionString) && !string.IsNullOrEmpty(APIBase);

        }

        private async void timer1_Tick(object sender, EventArgs e)
        {
            if (IsConfigurationSet)
            {
                await DoWork(SendFaciltyDataToAPI);
            }
            else
            {
                timer1.Stop();
            }
        }

        private async void Do_Work_Click(object sender, EventArgs e)
        {
            if (IsConfigurationSet)
            {
                MessageBox.Show("Please stop this application and update the settings");
                Application.Exit();
            }

            await DoWork(Sync);
        }

        async Task Sync()
        {
            if (this.ConnectionIsOkay())
            {
                await SendFaciltyDataToAPI();

                timer1.Start();
                MessageBox.Show("Sucessful");
            }
            else
            {
                MessageBox.Show("connection to the server/database is not okay");
            }
        }

        private async Task DoWork(Func<Task> work)
        {
            spinner_img.Visible = true;
            Cursor = Cursors.WaitCursor;

            await work();

            Cursor = Cursors.Arrow;
            spinner_img.Visible = false;
        }

        //TODO Refactor
        async Task SendFaciltyDataToAPI()
        {
            var facilityData = GetFacilities();
            try
            {
                HttpClient client = new();

                var dataToJson = JsonSerializer.Serialize(facilityData);

                var dataToStringContent = new StringContent(dataToJson, Encoding.UTF8, "application/json");

                var requestUri = new Uri($"{APIBase?.TrimEnd('/')}/UpdateFacilities");

                var response = await client.PostAsync(requestUri, content: dataToStringContent);
                if (!response.IsSuccessStatusCode)
                    MessageBox.Show("There was an issue posting the facility data to the api");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"There was an error while trying to post data to the api {ex.Message}");
            }

        }

        //TODO Refactor
        async Task SendTBIndicatorsDataToAPI()
        {
            var tBIndicators = GetTbIndicators();
            try
            {
                HttpClient client = new();

                var dataToJson = JsonSerializer.Serialize(tBIndicators);

                var dataToStringContent = new StringContent(dataToJson, Encoding.UTF8, "application/json");

                var requestUri = new Uri($"{APIBase?.TrimEnd('/')}/GetDashboardData");

                var response = await client.PostAsync(requestUri, content: dataToStringContent);
                if (!response.IsSuccessStatusCode)
                    MessageBox.Show("There was an issue posting some indicator data to the api");
            }
            catch (Exception ex)
            {
                MessageBox.Show($"There was an error while trying to post data to the api {ex.Message}");
            }
        }

        private bool ConnectionIsOkay()
        {
            MySqlConnection connect = new();

            try
            {
                connect = new MySqlConnection(ConnectionString);
                connect.Open(); 
                return true;

            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                return false;

            }
            finally
            {
                if (connect != null)
                    connect.Close();
            }

        }

        private string ReadQueryFromExternalFile(string fileName)
        {
            try
            {
                StringBuilder sb = new();
                string filePath = $"{AppDomain.CurrentDomain.BaseDirectory}{fileName}";

                using (StreamReader sr = new(filePath))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(line);
                    }
                }
                string allines = sb.ToString();
                return allines;
            }
            catch (Exception ex)
            {
                MessageBox.Show("please add the query to use");
                return "";
            }
            
        }

        public IEnumerable<TBIndicatorInfo> GetTbIndicators()
        {
            List<TBIndicatorInfo> indicators = new();
            using MySqlConnection connection = new(this.ConnectionString);
            using (connection)
            {

                MySqlCommand command = connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = ReadQueryFromExternalFile("tb_indicator_query.txt");


                try
                {
                    connection.Open();
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var ip = reader.GetString("IP");
                        var state = reader.GetString("State");
                        var facility_Name = reader.GetString("Facility_Name");
                        var Datim_Code = reader.GetString("Datim_Code");
                        var ART_Number = reader.GetString("ART_Number");
                        var Sex = reader.GetString("Sex");
                        var DOB = reader.GetString("DOB");
                        var Enrolment_Date = reader.GetString("Enrolment_Date");
                        var Weight_At_Enrolment = reader.GetString("Weight_At_Enrolment");
                        var WHO_Stage_AtEnrolment = reader.GetString("WHO_Stage_At-Enrolment");
                        var Symptoms = reader.GetString("Symptoms");
                        var CD4_Count_At_HIV_Diagnosis = reader.GetString("CD4_Count_At_HIV_Diagnosis");
                        var Date_Of_First_CD4 = reader.GetString("Date_Of_First_CD4");
                        var TB_Status_At_Enrolment = reader.GetString("TB_Status_At_Enrolment");
                        var RegimenAtARTStart = reader.GetString("RegimenAtARTStart");
                        var Prior_ART = reader.GetString("Prior_ART");
                        var ART_Start_Date = reader.GetString("ART_Start_Date");
                        var Current_ARV_Regimen = reader.GetString("Current_ARV_Regimen");
                        var Reasons_For_Stopping_ARVs = reader.GetString("Reasons_For_Stopping_ARVs");
                        var Current_Weight = reader.GetString("Current_Weight");
                        var Current_Weight_Date = reader.GetString("Current_Weight_Date");
                        var TPT_Start_Date = reader.GetString("TPT_Start_Date");
                        var TPT_Completion_Date = reader.GetString("TPT_Completion_Date");
                        var Current_TB_Staatus = reader.GetString("Current_TB_Staatus");
                        var CD4_Count_Current = reader.GetString("CD4_Count_Current");
                        var Date_Of_Current_CD4 = reader.GetString("Date_Of_Current_CD4");
                        var CurrentViralLoad = reader.GetString("CurrentViralLoad");
                        var DateofCurrentViralLoad = reader.GetString("DateofCurrentViralLoad");
                        var TB_Treatment_Completed = reader.GetString("TB_Treatment_Completed");

                        indicators.Add(
                            new TBIndicatorInfo()
                            {
                                IP = ip,
                                State = state,
                                Facility_Name = facility_Name,
                                Datim_Code = Datim_Code,
                                ART_Number = ART_Number,
                                Sex = Sex,
                                DOB = DOB,
                                Enrolment_Date = Enrolment_Date,
                                Weight_At_Enrolment = Weight_At_Enrolment,
                                WHO_Stage_AtEnrolment = WHO_Stage_AtEnrolment,
                                Symptoms = Symptoms,
                                CD4_Count_At_HIV_Diagnosis = CD4_Count_At_HIV_Diagnosis,
                                Date_Of_First_CD4 = Date_Of_First_CD4,
                                TB_Status_At_Enrolment = TB_Status_At_Enrolment,
                                RegimenAtARTStart = RegimenAtARTStart,
                                Prior_ART = Prior_ART,
                                ART_Start_Date = ART_Start_Date,
                                Current_ARV_Regimen = Current_ARV_Regimen,
                                Reasons_For_Stopping_ARVs = Reasons_For_Stopping_ARVs,
                                Current_Weight = Current_Weight,
                                Current_Weight_Date = Current_Weight_Date,
                                TPT_Start_Date = TPT_Start_Date,
                                TPT_Completion_Date = TPT_Completion_Date,
                                Current_TB_Staatus = Current_TB_Staatus,
                                CD4_Count_Current = CD4_Count_Current,
                                Date_Of_Current_CD4 = Date_Of_Current_CD4,
                                CurrentViralLoad = CurrentViralLoad,
                                DateofCurrentViralLoad = DateofCurrentViralLoad,
                                TB_Treatment_Completed = TB_Treatment_Completed,
                            }
                        );
                    }
                    reader.Close();
                    return indicators.Take(10);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }

            }
        }

        public IEnumerable<FacilityData> GetFacilities()
        {
            List<FacilityData> facilities = new();
            using MySqlConnection connection = new(this.ConnectionString);
            using (connection)
            {

                MySqlCommand command = connection.CreateCommand();
                command.CommandType = System.Data.CommandType.Text;
                command.CommandText = ReadQueryFromExternalFile("facilityData_sql_query.txt");


                try
                {
                    connection.Open();
                    MySqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        var patientId = reader.GetString("patient_id");
                        var name = reader.GetString("Name");
                        var facilityName = reader.GetString("Facility_Name");
                        var facilityId = reader.GetString("Facility_Id");
                        var DateCreated = reader.GetString("date_created");
                        var submission = reader.GetString("submissions");
                        var age = reader.GetString("Current_Age");
                        var gender = reader.GetString("Sex");
                        var state = reader.GetString("State");
                        var lga = reader.GetString("LGA");
                        var datimCode = reader.GetString("Datim_Code");
                        var implementingPartner = reader.GetString("Implementing_Partner");
                        var username = reader.GetString("Username");
                        facilities.Add(
                            new FacilityData()
                            {
                                PatientId = string.IsNullOrWhiteSpace(patientId) ? 0 : Convert.ToInt32(patientId),
                                Name = name ?? string.Empty,
                                FacilityName = facilityName ?? string.Empty,
                                FacilityId = facilityId ?? string.Empty,
                                DateCreated = DateCreated ?? string.Empty,
                                Submissions = string.IsNullOrWhiteSpace(submission) ? 0 : Convert.ToInt32(submission),
                                CurrentAge = string.IsNullOrWhiteSpace(age) ? (byte)0 : Convert.ToByte(age),
                                Sex = gender ?? string.Empty,
                                State = state ?? string.Empty,
                                LGA = lga ?? string.Empty,
                                DatimCode = datimCode ?? string.Empty,
                                ImplementingPartner = implementingPartner ?? string.Empty,
                                Username = username ?? string.Empty,
                            }
                        );
                    }
                    reader.Close();
                    return facilities.Take(10);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    throw;
                }

            }
        }

        private void settings_Click(object sender, EventArgs e)
        {
            // Create a new instance of the Form2 class
            var settingsForm = new SettingsForm();

            // Show the settings form
            settingsForm.Show();
        }

        private async void indicator_btn_Click(object sender, EventArgs e)
        {
            if (IsConfigurationSet)
            {
                MessageBox.Show("Please stop this application and update the settings");
                Application.Exit();
            }
            await DoWork(SyncIndicator);


        }

        async Task SyncIndicator()
        {
            if (this.ConnectionIsOkay())
            {
                await SendTBIndicatorsDataToAPI();
                MessageBox.Show("Sucessful");
            }
            else
            {
                MessageBox.Show("connection to the server/database is not okay");
            }
        }
    }
}
