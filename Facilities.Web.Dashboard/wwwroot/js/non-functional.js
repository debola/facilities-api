﻿
$(function() {

	'use strict';

	// Form

	var contactForm = function() {
		let baseUrl = `https://localhost:5001/api/Facilities`
		let name = $('#name')
		let state = $('#state-select')
		let facilityName = $('#facility-select')
		let complaint = $('#complaint-select')
		let messageForm = $('#messageForm')
		let complaintMessage = $('#message')

		if ($('#contactForm').length > 0) {
			$("#contactForm").validate({
				rules: {
					name: "required",
					state: "required",
					facilityName: "required",
					complaint: "required",
					complaintMessage: {
						required: true,
						minlength: 10
					}
				},
				messages: {
					name: "Please enter your name",
					email: "Please enter a valid email address",
					complaintMessage: "Please enter a message"
				},
				dataTable: $('#reportTable').DataTable({

					ajax: {
						url: `${baseUrl}/ReportLogs`,
						dataSrc: ''
					},
					columns: [
						{ data: 'name' },
						{ data: 'state' },
						{ data: 'facilityName' },
						{ data: 'complaint' },
						{ data: 'complaintMessage' },
						{ data: 'entered' },
					],
				}),
				populateFacilityDropdown: $('#state-select').on('change', function(){
					let selectedState = $(this).val()
					if (selectedState) {
						$.ajax({
							type: "GET",
							url: `${baseUrl}?state=${selectedState}`,
							success: function (response) {
								if (response) {
									var facilityDropdown = $("#facility-select")
									populateDropdown(facilityDropdown, response)
								}
							}
						})
                    }
				}),
				submitHandler: function() {
					var $submit = $('.submitting'),
						waitText = 'Submitting...';
					var data = {
						name: name.val(),
						state: state.val(),
						facilityName: facilityName.val(),
						complaint: complaint.val(),
						complaintMessage: complaintMessage.val() ?? "",
					}
					var dataToJson = JSON.stringify(data)
					$.ajax({
						type: "POST",
						url: `${baseUrl}/ReportComplaint`,
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						data: dataToJson,
						beforeSend: function () {
							$submit.css('display', 'block').text(waitText);
						},
						success: function (msg) {
							if (msg == 'success') {
								$('#form-message-warning').hide();
								$('#form_title').hide();
								setTimeout(function () {
									$('#contactForm').fadeOut();
								}, 1000);
								setTimeout(function () {
									$('#form-message-success').fadeIn();
								}, 1400);

							} else {
								$('#form-message-warning').html(msg);
								$('#form-message-warning').fadeIn();
								$submit.css('display', 'none');
							}
						},
						error: function (e) {
							$('#form-message-warning').html("Something went wrong. Please try again.");
							$('#form-message-warning').fadeIn();
							$submit.css('display', 'none');
						}
					});
				}

			});
		}
	};
	contactForm();
	
	function populateDropdown(dropdown, data) {
		dropdown.empty();
		dropdown.append(new Option('Select', ''))
		data.forEach((item) => {
			dropdown.append(new Option(item.name, item.datimCode));
		});
	}
});