﻿namespace Monitor.Model
{
    /// <summary>
    /// Models the facilities data that is received from the facility database and sent to the api
    /// </summary>
    /// <param name="Patient_Id"></param>
    /// <param name="Name"></param>
    /// <param name="Date_Created"></param>
    /// <param name="Submissions"></param>
    public class FacilityData
    {
        public string DatimCode { get; set; }
        public string FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string ImplementingPartner { get; set; }
        public string LGA { get; set; }
        public string State { get; set; }
        public int Submissions { get; set; }
        public string Name { get; set; }
        public string DateCreated { get; set; }
        public int PatientId { get; set; }
        public string Sex { get; set; }
        public byte CurrentAge { get; set; }
        public string Username { get; set; }
    }

    /// <summary>
    /// Models the data that is received from the database to create the <see cref="DashboardDataInfo"/>
    /// </summary>
    public class FacilityInfo
    {
        public int FacilityId { get; set; }
        public string DatimCode { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public int Submissions { get; set; }
        public DataAbstractor[] DataAbstractors { get; set; }
        public PatientInfo[] Patients { get; set; }
    }

    
    public class PatientInfo
    {
        public int PatientId { get; set; }
        public string Name { get; set; }
        public byte Age { get; set; }
        public DateTime TimeStamp { get; set; }
        public Gender Gender { get; set; }
    }

    /// <summary>
    /// Models the dashboard data for each chart
    /// </summary>
    public class DashboardDataInfo
    {
        public SubmissionDistribution SubmissionDistribution { get; set; } = new();
        
        /// <summary>
        /// This holds the grouped age range per state and how many patients fall under them 
        /// </summary>
        public IEnumerable<AgeDistribution> AgeDistribution { get; set; } = Enumerable.Empty<AgeDistribution>();
        public IEnumerable<PatientDistribution> PatientDistribution { get; set; } = Enumerable.Empty<PatientDistribution>();
        public IEnumerable<DataAbstractorDistribution> DataAbstractorsFacilityCountDistribution {get; set;} = Enumerable.Empty<DataAbstractorDistribution>();
        public IEnumerable<FormSubmission> FormDistribution {get; set;} = Enumerable.Empty<FormSubmission>();
    }


    public class SubmissionDistribution
    {
        private const int EXPECTED_SUBMISSION_PER_FACILITY_PER_DAY = 20;

        private decimal AGGREGATED_EXPECTED_SUBMISSION_PER_STATE_PER_DAY => EXPECTED_SUBMISSION_PER_FACILITY_PER_DAY * FacilitySubmissions.Count();
        
        public IEnumerable<FacilitySubmission> FacilitySubmissions { get; set; } = Enumerable.Empty<FacilitySubmission>();
        public int AggregatedSubmissionsPerFacility => FacilitySubmissions.Sum(s => s.Submissions);
        public decimal AggregatedNonSubmissions => AGGREGATED_EXPECTED_SUBMISSION_PER_STATE_PER_DAY - AggregatedSubmissionsPerFacility;
        public decimal PercentageSubmitted => AggregatedSubmissionsPerFacility == 0 ? 0
                        : (AggregatedSubmissionsPerFacility / AGGREGATED_EXPECTED_SUBMISSION_PER_STATE_PER_DAY * 100);
        public decimal PercentageNotSubmitted => 100 - PercentageSubmitted;
    }

    public class FormSubmission
    {
        public string FormName { get; set; }
        public string FacilityName { get; set; }
        public int Submissions { get; set; }
    }
    public class FormDTO
    {
        public string State { get; set; }
        public int FormId { get; set; }
    }

    public class FacilityFilterDTO
    {
        public string State { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
    public class FacilityDTO
    {
        public string DatimCode { get; set; }
        public string Name { get; set; }
    }
    public class PatientDistribution
    {
        public string FacilityName { get; set; }
        public int MaleCount { get; set; }
        public int FemaleCount { get; set; }
        public int TotalPatientCount { get; set; }

    }
    public class FacilitySubmission
    {
        public string FacilityName { get; set; }
        public int Submissions { get; set; }
        public int NonSubmissions => 20 - Submissions;
    }

    public class AgeDistribution
    {
        public string Range { get; set; }
        public int MaleCount { get; set; }
        public int FemaleCount { get; set; }
    }

    public class DataAbstractor
    {
        public string FacilityName { get; set; }
        public int SubmissionCount { get; set; }
        public string AbstractorName { get; set; }

    }

    public class DataAbstractorDistribution
    {
        public string FacilityName { get; set; }
        public string AbstractorName { get; set; }
        public string DatimCode { get; set; }
        public int AbstractorCount { get; set; }
    }

    //use json property for these properties and rename them to pascal cases
    public class DataAbstractorInfoDistribution
    {
        public string FacilityName { get; set; }
        public string AbstractorName { get; set; }
        //public int FormSubmissionCount { get; set; }
        public int TB_Screening_Count { get; set; }
        public int TB_ReferralForm_Count { get; set; }
        public int TB_Index_Patient_Ivestigation_Count { get; set; }
        public int Presumptive_TB_Register_Count { get; set; }
        public int TB_Laboratory_Form_Count { get; set; }
        public int Specimen_Request_Form_Count { get; set; }
        public int Specimen_Result_Form_Count { get; set; }
        public int DR_TB_Treartment_Register_Count { get; set; }
        public int TB_Patient_Referral_Transfer_Count { get; set; }
        public int TB_treatment_initiation_card_Count { get; set; }
        public int TB_treament_monitoring_form_Count { get; set; }
        public int TB_interruption_tracking_form_Count { get; set; }
        public int DR_TBInPatient_Discharge_Form_Count { get; set; }
        public int TB_LGA_Health_Facility_Register_Count { get; set; }

    }

    public class TBIndicatorInfo
    {
        public string IP { get; set; }
        public string State { get; set; }
        public string Facility_Name { get; set; }
        public string Datim_Code { get; set; }
        public string ART_Number { get; set; }
        public string Sex { get; set; }
        public string DOB { get; set; }
        public string Enrolment_Date { get; set; }
        public string Weight_At_Enrolment { get; set; }
        public string WHO_Stage_AtEnrolment { get; set; }
        public string Symptoms { get; set; }
        public string CD4_Count_At_HIV_Diagnosis { get; set; }
        public string Date_Of_First_CD4 { get; set; }
        public string TB_Status_At_Enrolment { get; set; }
        public string RegimenAtARTStart { get; set; }
        public string Prior_ART { get; set; }
        public string ART_Start_Date { get; set; }
        public string Current_ARV_Regimen { get; set; }
        public string Reasons_For_Stopping_ARVs { get; set; }
        public string Current_Weight { get; set; }
        public string Current_Weight_Date { get; set; }
        public string TPT_Start_Date { get; set; }
        public string TPT_Completion_Date { get; set; }
        public string Current_TB_Staatus { get; set; }
        public string CD4_Count_Current { get; set; }
        public string Date_Of_Current_CD4 { get; set; }
        public string CurrentViralLoad { get; set; }
        public string DateofCurrentViralLoad { get; set; }
        public string TB_Treatment_Completed { get; set; }
    }

    public class ComplaintDTO
    {
        public string Name { get; set; }
        public string State { get; set; }
        public string FacilityName { get; set; }
        public string Complaint { get; set; }
        public string ComplaintMessage { get; set; }
    }

    public class ComplaintResult :ComplaintDTO
    {
        public string Entered { get; set; }
    }

    public class LoginDTO
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
    public class LoginResult
    {
        public bool IsSuccessful { get; set; }
        public int Role { get; set; }
    }

}