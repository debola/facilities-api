﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Facilities.API.Migrations
{
    public partial class adddataabstractortofacility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DataAbstractor",
                table: "Facility",
                type: "nvarchar(max)",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DataAbstractor",
                table: "Facility");
        }
    }
}
