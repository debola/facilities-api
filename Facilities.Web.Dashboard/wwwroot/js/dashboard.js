﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

let baseUrl = `https://localhost:5001/api/Facilities`
let stackedChart;
let pieChart;
let chartContent = $('.chart-content')
let stateDropdown = $('#state-select')
let tbScreeningDropdown = $('#tbScreening-select')
let tbLab = $('#tbLab-select')
let tbTreatment = $('#tbTreatment-select')
let dataAbstractorSelect = $(`#facility-ab-select`);
let formPerAbstractor = $(`#formPerAbstractor`);
let selectedState = "Benue";
let defaultTbScreening = 98
let defaultTbLab = 102
let defaultTbTreatment = 82
let startDate = null;
let endDate = null;
let validation = $('#date-validation');

const colors = {
    black: '#000',
    cyan: 'rgba(0,255,204)',
    purple: 'rgb(160,32,240)',
    red: 'rgb(255, 99, 132)',
    pink: 'rgb(255,192,203)',
    green: 'rgb(0,255,0)',
    orange: 'rgb(255,165,0)',
    deepgreen: '#216952',
    violet: '#CF9FFF',
    brown: '#900C3F',
    lemon: '#DAF7A6',
    red_red: '#FC0319',
    yellow: '#FCF002',
    blue: '#0411F9',
}

$(document).ready(function () {
    LoadDashboard("Benue")
    loadDefaultFormChart();
}
);

function loadDefaultFormChart() {
    LoadFormChart(defaultTbScreening, selectedState, "tbScreening")
    LoadFormChart(defaultTbLab, selectedState, 'tbLaboratory')
    LoadFormChart(defaultTbTreatment, selectedState, 'tbTreatment')
}

function resetFormSelectOption() {
    tbScreeningDropdown.val(defaultTbScreening)
    tbLab.val(defaultTbLab)
    tbTreatment.val(defaultTbTreatment)
}

stateDropdown.on('change', function () {

    selectedState = $(this).val()
    formPerAbstractor.hide()
    resetFormSelectOption()
    loadDefaultFormChart()

});

$('#filterByDate').on("click", function (e) {
    if (!validation.attr('hidden'))
        validation.prop('hidden', true)
    LoadDashboard(selectedState, startDate, endDate)
    startDate = endDate = null
});

function LoadDashboard(state, startDate, endDate) {

    var dateIsSet = endDate && startDate

    if (dateIsSet && (endDate < startDate)) {
        validation.removeAttr('hidden')  //todo move to go button click
        return
    }
    let start = dateIsSet ? startDate.toJSON() : ''
    let end = dateIsSet ? endDate.toJSON() : '';
    $.ajax({
        type: "GET",
        url: `${baseUrl}/GetDashboardData?State=${selectedState}&StartDate=${start}&EndDate=${end}`,
        success: function (response) {
            if (response) {
                toggleChartVisibility(true)
                populateDropdown(dataAbstractorSelect, response.dataAbstractorsFacilityCountDistribution)
                BuildSubmissionTabChart(response.submissionDistribution);
                BuildPatientDistributionChart(response.patientDistribution)
                BuildPatientAgeRangeChart(response.ageDistribution)
                BuildDataAbstractorsTab(response.dataAbstractorsFacilityCountDistribution)
                $('.highcharts-credits').remove() //so sorry I had to do this 😈

            }
            else {
                toggleChartVisibility(false)
            }
        }
    });
}

function toggleChartVisibility(show = false) {
    if (show) {
        $("#facilitySubmissionPercent").show()
        $("#facilitySubmission").show()
        $("#patientDistribution").show()
        $("#ageDistribution").show()
        $("#dataAbstractor").show()
        $("#formPerAbstractor").show()
    }
    else {
        $("#facilitySubmissionPercent").hide()
        $("#facilitySubmission").hide()
        $("#patientDistribution").hide()
        $("#ageDistribution").hide()
        $("#dataAbstractor").hide()
        $("#formPerAbstractor").hide()
    }
}

dataAbstractorSelect.on('change', function () {

    selectedFacility = $(this).val()
    if (selectedFacility != '') {
        formPerAbstractor.show()
        LoadAbstractorChart(selectedFacility)
        $('.highcharts-credits').remove()
    }
        

});

tbScreeningDropdown.on('change', function () {

    selectedForm = $(this).val()
    LoadFormChart(selectedForm, selectedState, 'tbScreening')

});
tbLab.on('change', function () {

    selectedForm = $(this).val()
    LoadFormChart(selectedForm, selectedState, 'tbLaboratory')

});
tbTreatment.on('change', function () {

    selectedForm = $(this).val()
    LoadFormChart(selectedForm, selectedState, 'tbTreatment')

});


function LoadFormChart(formId, state, id) {

    $.ajax({
        type: "GET",
        url: `${baseUrl}/GetForms?State=${state}&FormId=${formId}`,
        success: function (response) {

            //TODO refactor charts
            if (response) {
                $(`#${id}`).show()
                BuildFormTab(response, id)

            } else {
                $(`#${id}`).hide()
            }
        }
    });
}

function LoadAbstractorChart(datimCode) {
    var nameFormCount = [{ abstractorName: "Kunle", TBFormCount: 4, TB_RequestCount: 8, TB_GoatForm: 7 }, { abstractorName: "Tunji", TBHCount: 5, LassaForm: 12, CoughForm: 6 }, { abstractorName: "Bola",  DR_TBCount: 8 }]
    //var abstractorName = ["Sade", "Tunji", "Bola"]
    //var form = ["TBForm", "TB_Request", "TB_GoatForm", "TBHForm", "LassaForm", "CoughForm", "DR_TB"]
    var data = nameFormCount
    $.ajax({
        type: "GET",
        url: `${baseUrl}/GetDataAbstractorInfo?datimCode=${datimCode}`,//localhost:5001/api/Facilities/GetDataAbstractorInfo?datimCode
        success: function (response) {

            //TODO refactor charts
            if (response) {
                console.log(response)
                LoadAbstractorSubmissionChart(response, 'formPerAbstractor')
            } else {
            }
        }
    });
}


function LoadAbstractorSubmissionChart(data, id) {

    let abstractorNames = []
    TB_Screening = []
    TB_ReferralForm = []
    TB_Index_Patient_Ivestigation = []
    Presumptive_TB_Register = []
    TB_Laboratory_Form = []
    Specimen_Request_Form = []
    Specimen_Result_Form = []
    DR_TB_Treartment_Register = []
    TB_Patient_Referral_Transfer = []
    TB_treatment_initiation_card = []
    TB_treament_monitoring_form = []
    TB_interruption_tracking_form = []
    DR_TBInPatient_Discharge_Form = []
    TB_LGA_Health_Facility_Register = []

    data.forEach(item => {
        abstractorNames.push(item.abstractorName);
        TB_Screening.push(item.tB_Screening_Count)
        TB_ReferralForm.push(item.tB_ReferralForm_Count)
        TB_Index_Patient_Ivestigation.push(item.tB_Index_Patient_Ivestigation_Count)
        Presumptive_TB_Register.push(item.presumptive_TB_Register_Count)
        TB_Laboratory_Form.push(item.TB_laboratory_Form_Count)
        Specimen_Request_Form.push(item.specimen_Request_Form_Count)
        Specimen_Result_Form.push(item.specimen_Result_Form_Count)
        DR_TB_Treartment_Register.push(item.dR_TB_Treartment_Register_Count)
        TB_Patient_Referral_Transfer.push(item.tB_Patient_Referral_Transfer_Count)
        TB_treatment_initiation_card.push(item.tB_treatment_initiation_card_Count)
        TB_treament_monitoring_form.push(item.tB_treament_monitoring_form_Count)
        TB_interruption_tracking_form.push(item.tB_interruption_tracking_form_Count)
        DR_TBInPatient_Discharge_Form.push(item.dR_TBInPatient_Discharge_Form_Count)
        TB_LGA_Health_Facility_Register.push(item.tB_LGA_Health_Facility_Register_Count)
    })

    Highcharts.chart(id, {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Form submission per abstractor'
        },
        xAxis: {
            categories: abstractorNames
        },
        yAxis: {
            title: {
                text: 'Total form submissions'
            },
            tickInterval: 1
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [
            {
                name: 'TB Screening',
                data: TB_Screening,
                color: colors.black
            },
            {
                name: 'TB Referral Form',
                data: TB_ReferralForm,
                color: colors.green
            },
            {
                name: 'TB Index Patient Contact Ivestigation',
                data: TB_Index_Patient_Ivestigation,
                color: colors.orange
            },
            {
                name: 'Presumptive TB Register',
                data: Presumptive_TB_Register,
                color: colors.pink
            },
            {
                name: 'TB Laboratory Form',
                data: TB_Laboratory_Form,
                color: colors.red
            },
            {
                name: 'Specimen Request Form',
                data: Specimen_Request_Form,
                color: colors.cyan
            },
            {
                name: 'Specimen Result Form',
                data: Specimen_Result_Form,
                color: colors.deepgreen
            },
            {
                name: 'DR TB Treartment Register',
                data: DR_TB_Treartment_Register,
                color: colors.purple
            },
            {
                name: 'TB patient Referral or Transfer',
                data: TB_Patient_Referral_Transfer,
                color: colors.blue
            },
            {
                name: 'TB treatment initiation card',
                data: TB_treatment_initiation_card,
                color: colors.brown
            },
            {
                name: 'TB treament monitoring form',
                data: TB_treament_monitoring_form,
                color: colors.lemon
            },
            {
                name: 'TB interruption tracking form',
                data: TB_interruption_tracking_form,
                color: colors.gold
            },
            {
                name: 'DR-TB in -patient Discharge Form',
                data: DR_TBInPatient_Discharge_Form,
                color: colors.red_red
            }, {
                name: 'TB LGA Health Facility Register',
                data: TB_LGA_Health_Facility_Register,
                color: colors.yellow
            },
        ]
    });
}


function BuildSubmissionTabChart(data) {

   BuildSubmissionDistributionPercentagePieChart(data);

    BuildSubmissionDistributionBarChart(data)
}


function BuildFormTab(data, id) {

    let facilities = [];
    let submissions = [];

    data.forEach(item => {
        facilities.push(item.facilityName);
        submissions.push(item.submissions)
    })

    Highcharts.chart(id, {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Form submissions per facilities'
        },
        xAxis: {
            categories: facilities
        },
        yAxis: {
            title: {
                text: 'Total submissions'
            },
            tickInterval: 1
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: "Total submissions",
            data: submissions,
            color: colors.cyan
        }]
    });
}

function BuildDataAbstractorsTab(data) {
    let facilities = [];
    let abstractorCount = [];

    data.forEach(item => {
        facilities.push(item.facilityName);
        abstractorCount.push(item.abstractorCount)
    })

    Highcharts.chart('dataAbstractor', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Data abstractors in facilities'
        },
        xAxis: {
            categories: facilities
        },
        yAxis: {
            title: {
                text: 'Total abstractors'
            }
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Data abstractors',
            data: abstractorCount,
            color: colors.orange
        }]
    });
}

function BuildPatientAgeRangeChart(data) {
   
    let ageRange = [];
    let maleDataCount = [];
    let femaleDataCount = [];
    let count = [];

    data.forEach(item => {
        ageRange.push(item.range);
        maleDataCount.push(-Math.abs(item.maleCount));
        femaleDataCount.push(item.femaleCount);
        count.push(item.count);
    })
    Highcharts.chart('ageDistribution', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Patients submission disagregated by age and gender'
        },
        xAxis: [{
            categories: ageRange,
            reversed: false,
            labels: {
                step: 1
            },
            accessibility: {
                description: 'Age (male)'
            }
        }, { // mirror axis on right side
            opposite: true,
            reversed: false,
            categories: ageRange,
            linkedTo: 0,
            labels: {
                step: 1
            },
            accessibility: {
                description: 'Age (female)'
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value);
                }
            },
            tickInterval: 1
        },
        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                    Math.abs(this.point.y);
            }
        },
        plotOptions: {
            bar: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        },
        series: [{
            name: 'Male',
            data: maleDataCount,
            color: colors.purple
        }, {
            name: 'Female',
            data: femaleDataCount,
            color: colors.pink
        }]
    });
}
function BuildPatientDistributionChart(data) {
    let facilities = [];
    let malePatients = [];
    let femalePatients = [];

    data.forEach(item => {

        facilities.push(item.facilityName);
        malePatients.push(item.maleCount);
        femalePatients.push(item.femaleCount);
    })

    Highcharts.chart('patientDistribution', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Patients submission disagregated by gender'
        },
        xAxis: {
            categories: facilities
        },
        yAxis: {
            title: {
                text: 'Number of Patients'
            },
            tickInterval: 1
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Male',
            data: malePatients,
            color: colors.purple
        }, {
            name: 'Female',
            data: femalePatients,
            color: colors.pink
        }]
    });
}

function BuildSubmissionDistributionPercentagePieChart(data) {

    let labels = { submitted: "Submitted", unsubmitted: "Not Submitted" };
    let pieData = { submitted: data.percentageSubmitted, unsubmitted: data.percentageNotSubmitted };

    Highcharts.chart('facilitySubmissionPercent', {

        chart: {
            type: 'gauge',
            plotBackgroundColor: null,
            plotBackgroundImage: null,
            plotBorderWidth: 0,
            plotShadow: false,
            height: '80%'
        },

        title: {
            text: 'Entry submissions'
        },

        pane: {
            startAngle: -90,
            endAngle: 89.9,
            background: null,
            center: ['50%', '75%'],
            size: '110%'
        },

        // the value axis
        yAxis: {
            min: 0,
            max: 100,
            tickPixelInterval: 72,
            tickPosition: 'inside',
            tickColor: Highcharts.defaultOptions.chart.backgroundColor || '#FFFFFF',
            tickLength: 20,
            tickWidth: 2,
            minorTickInterval: null,
            labels: {
                distance: 20,
                style: {
                    fontSize: '14px'
                }
            },
            plotBands: [{
                from: 0,
                to: 40,
                color: '#DF5353', // green
                thickness: 20
            }, {
                from: 40,
                to: 70,
                color: '#DDDF0D', // yellow
                thickness: 20
            }, {
                from: 70,
                to: 100,
                color: '#55BF3B', // red
                thickness: 20
            }]
        },

        series: [{
            name: 'Submitted',
            data: [pieData.submitted],
            tooltip: {
                valueSuffix: ' submitted'
            },
            dataLabels: {
                format: '{y}% submitted',
                borderWidth: 0,
                color: (
                    Highcharts.defaultOptions.title &&
                    Highcharts.defaultOptions.title.style &&
                    Highcharts.defaultOptions.title.style.color
                ) || '#333333',
                style: {
                    fontSize: '16px'
                }
            },
            dial: {
                radius: '80%',
                backgroundColor: 'gray',
                baseWidth: 12,
                baseLength: '0%',
                rearLength: '0%'
            },
            pivot: {
                backgroundColor: 'gray',
                radius: 6
            }

        }]
    });
}


function BuildSubmissionDistributionBarChart(data) {
    let facilities = [];
    let submitted = [];
    let unsubmitted = [];

    data.facilitySubmissions.forEach(item => {
        facilities.push(item.facilityName);
        submitted.push(item.submissions)
        unsubmitted.push(item.nonSubmissions)
    })

    Highcharts.chart('facilitySubmission', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Target against Submissions',
            align: 'center'
        },
        xAxis: {
            categories: facilities
        },
        yAxis: {
            min: 0,
            title: {
                text: 'No of submissions'
            },
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: 'black',
                    textOutline: 'none'
                }
            }
        },
        legend: {
            align: 'center',
            x: 70,
            verticalAlign: 'bottom',
            y: 5,
            floating: false,
            backgroundColor: 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{
            name: 'Submitted',
            data: submitted,
            color: colors.cyan
        }, {
            name: 'Unsubmitted',
            data: unsubmitted,
            color: colors.red
        }]
    });
}

function populateDropdown(dropdown, data) {
    dropdown.empty();
    dropdown.append(new Option('Select', ''))
    data.forEach((item) => {
        dropdown.append(new Option(item.facilityName, item.datimCode));
    });
}

//date range
const startRange = document.querySelector('input[name="range-start"]');
const endRange = document.querySelector('input[name="range-end"]');
const datepickerStart = new Datepicker(startRange, {
    // ...options
});
const datepickerEnd = new Datepicker(endRange, {
    // ...options
});

const startElem = $('#start');
const endElem = $('#end');

startElem.on('changeDate', function (e) {
    let date = e.detail.date;
    if (date.getMonth)
        startDate = date
});
endElem.on('changeDate', function (e) {
    let date = e.detail.date;
    if (date.getMonth)
        endDate = date
});
