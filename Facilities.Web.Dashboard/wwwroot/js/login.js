﻿$(function () {

	'use strict';

	// Form

	var loginForm = function () {
		let baseUrl = `https://localhost:5001/api/Facilities`
		let username = $('#username')
		let password = $('#password')

		if ($('#loginForm').length > 0) {
			$("#loginForm").validate({
				rules: {
					username: "required",
					password: "required",
				},
				messages: {
					username: "Please enter your username",
					password: "Please enter your password"
				},
				submitHandler: function () {
					var data = {
						username: username.val(),
						password: password.val()
					}

					var dataToJson = JSON.stringify(data)
					$.ajax({
						type: "POST",
						url: `${baseUrl}/Login`,
						headers: {
							'Accept': 'application/json',
							'Content-Type': 'application/json'
						},
						data: dataToJson,
						success: function (response) {
							if (response && response.isSuccessful) {
								if (response.role == 1)
									location.href = "tb-indicators"
								if (response.role == 2)
									location.href = "report-log"
							}
							else {
								$('#bad-login-message').html("Invalid login credentials")
                            }
						},
						error: function (e) {
						}
					});
				}

			});
		}
	};

	loginForm();
});