﻿using Monitor.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Monitor.WF
{
    public static class Helper
    {
        private static string ReadFromExternalFile(string fileName)
        {
            try
            {
                StringBuilder sb = new();
                string filePath = $"{AppDomain.CurrentDomain.BaseDirectory}{fileName}";

                using (StreamReader sr = new(filePath))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(line);
                    }
                }
                string allines = sb.ToString();
                return allines;
            }
            catch (Exception ex)
            {
                throw;
            }

        }

        public static ConfigSettings GetConfigurationSettings()
        {
            try
            {
                //read fromjson file and populate form
                var configurationFile = ReadFromExternalFile("settings.json");
                var configSettings = JsonSerializer.Deserialize<ConfigSettings>(configurationFile);
                return configSettings;
                
            }
            catch (Exception)
            {
                return new ConfigSettings();
            }
        }
    }
}
