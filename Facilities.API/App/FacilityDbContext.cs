﻿using Monitor.Model;
using Microsoft.EntityFrameworkCore;

namespace Facilities.API.App
{
    public class FacilityDbContext : DbContext
    {
        public FacilityDbContext(DbContextOptions<FacilityDbContext> dbContextOptions) : base(dbContextOptions)
        { }
        public DbSet<Facility> Facility { get; set; }
        public DbSet<TBIndicator> TBIndicator { get; set; }
        public DbSet<Complaints> Complaints { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Facility>().HasKey(table => new
            {
                table.Id,
                table.DatimCode,
                table.PatientId,
                table.FormId,
            });

            modelBuilder.Entity<Facility>().Property(f => f.Id)
            .ValueGeneratedOnAdd();

            modelBuilder.Entity<TBIndicator>().HasKey(table => new
            {
                table.Id,
                table.DatimCode,
                table.ART_Number,
            });
            modelBuilder.Entity<TBIndicator>().Property(f => f.Id)
            .ValueGeneratedOnAdd();

            modelBuilder.Entity<Complaints>().HasKey(c => c.Id);
            modelBuilder.Entity<Complaints>().Property(c => c.Id)
            .ValueGeneratedOnAdd();
        }
    }
}
