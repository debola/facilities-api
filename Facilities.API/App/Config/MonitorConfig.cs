﻿namespace Facilities.API.App.Config
{
    public class MonitorConfig
    {
        public Form[] Forms { get; set; }
    }

    public class Form
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
