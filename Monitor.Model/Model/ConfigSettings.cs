﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor.Model.Model
{
    public class ConfigSettings
    {
        public string Server { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public int Port { get; set; }

        public override string ToString()
        {
            
            return String.IsNullOrWhiteSpace(Server) ? "" : $"Server={Server}; Port={Port}; Database={Database}; Uid={Username}; Pwd={Password};";
        }
    }
}
