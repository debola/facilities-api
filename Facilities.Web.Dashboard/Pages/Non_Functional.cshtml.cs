using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Monitor.Model;

namespace Facilities.Web.Dashboard.Pages
{
    public class Non_FunctionalModel : PageModel
    {
        private readonly ILogger<Non_FunctionalModel> _logger;
        public Non_FunctionalModel(ILogger<Non_FunctionalModel> logger, IHttpClientFactory httpClientFactory)
        {
            _logger = logger;
        }

        public async Task OnGet()
        {
            
        }
    }
}
