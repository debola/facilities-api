﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Facilities.API.Migrations
{
    public partial class Addschema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Facility",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FormId = table.Column<int>(type: "int", nullable: false),
                    PatientId = table.Column<int>(type: "int", nullable: false),
                    DatimCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    FacilityId = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    FacilityName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ImplementingPartner = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    LGA = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Submissions = table.Column<int>(type: "int", nullable: false),
                    FormType = table.Column<int>(type: "int", nullable: false),
                    FormName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Sex = table.Column<byte>(type: "tinyint", nullable: false),
                    CurrentAge = table.Column<byte>(type: "tinyint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Facility", x => new { x.Id, x.DatimCode, x.PatientId, x.FormId });
                });

            migrationBuilder.CreateTable(
                name: "TBIndicator",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    DatimCode = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    ART_Number = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    IP = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    State = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Facility_Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Sex = table.Column<byte>(type: "tinyint", nullable: false),
                    DOB = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Enrolment_Date = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Weight_At_Enrolment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    WHO_Stage_AtEnrolment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Symptoms = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CD4_Count_At_HIV_Diagnosis = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Date_Of_First_CD4 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TB_Status_At_Enrolment = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RegimenAtARTStart = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Prior_ART = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ART_Start_Date = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Current_ARV_Regimen = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Reasons_For_Stopping_ARVs = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Current_Weight = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Current_Weight_Date = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TPT_Start_Date = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TPT_Completion_Date = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Current_TB_Status = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CD4_Count_Current = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Date_Of_Current_CD4 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    CurrentViralLoad = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    DateofCurrentViralLoad = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    TB_Treatment_Completed = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TBIndicator", x => new { x.Id, x.DatimCode, x.ART_Number });
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Facility");

            migrationBuilder.DropTable(
                name: "TBIndicator");
        }
    }
}
