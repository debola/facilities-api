﻿namespace Monitor.WF
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._sever = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.server_name = new System.Windows.Forms.TextBox();
            this.uid = new System.Windows.Forms.TextBox();
            this.pwrd = new System.Windows.Forms.TextBox();
            this.database = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cancel_btn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.port_number = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.port_number)).BeginInit();
            this.SuspendLayout();
            // 
            // _sever
            // 
            this._sever.AutoSize = true;
            this._sever.Location = new System.Drawing.Point(12, 39);
            this._sever.Name = "_sever";
            this._sever.Size = new System.Drawing.Size(48, 20);
            this._sever.TabIndex = 0;
            this._sever.Text = "server";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "username";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 130);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 20);
            this.label3.TabIndex = 2;
            this.label3.Text = "password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 174);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "database";
            // 
            // server_name
            // 
            this.server_name.Location = new System.Drawing.Point(104, 39);
            this.server_name.Name = "server_name";
            this.server_name.Size = new System.Drawing.Size(195, 27);
            this.server_name.TabIndex = 4;
            this.server_name.TextChanged += new System.EventHandler(this.server_name_TextChanged);
            // 
            // uid
            // 
            this.uid.Location = new System.Drawing.Point(104, 84);
            this.uid.Name = "uid";
            this.uid.Size = new System.Drawing.Size(195, 27);
            this.uid.TabIndex = 5;
            this.uid.TextChanged += new System.EventHandler(this.uid_TextChanged);
            // 
            // pwrd
            // 
            this.pwrd.Location = new System.Drawing.Point(104, 127);
            this.pwrd.Name = "pwrd";
            this.pwrd.Size = new System.Drawing.Size(195, 27);
            this.pwrd.TabIndex = 6;
            this.pwrd.TextChanged += new System.EventHandler(this.pwrd_TextChanged);
            // 
            // database
            // 
            this.database.Location = new System.Drawing.Point(104, 173);
            this.database.Name = "database";
            this.database.Size = new System.Drawing.Size(195, 27);
            this.database.TabIndex = 7;
            this.database.TextChanged += new System.EventHandler(this.database_TextChanged);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.Highlight;
            this.button1.Location = new System.Drawing.Point(204, 252);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(94, 29);
            this.button1.TabIndex = 9;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // cancel_btn
            // 
            this.cancel_btn.BackColor = System.Drawing.Color.Firebrick;
            this.cancel_btn.Location = new System.Drawing.Point(104, 252);
            this.cancel_btn.Name = "cancel_btn";
            this.cancel_btn.Size = new System.Drawing.Size(94, 29);
            this.cancel_btn.TabIndex = 10;
            this.cancel_btn.Text = "Cancel";
            this.cancel_btn.UseVisualStyleBackColor = false;
            this.cancel_btn.Click += new System.EventHandler(this.cancel_btn_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(12, 225);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 20);
            this.label5.TabIndex = 12;
            this.label5.Text = "port";
            // 
            // port_number
            // 
            this.port_number.Location = new System.Drawing.Point(104, 221);
            this.port_number.Maximum = new decimal(new int[] {
            5000000,
            0,
            0,
            0});
            this.port_number.Name = "port_number";
            this.port_number.Size = new System.Drawing.Size(195, 27);
            this.port_number.TabIndex = 8;
            this.port_number.ValueChanged += new System.EventHandler(this.port_number_ValueChanged);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(324, 334);
            this.Controls.Add(this.port_number);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cancel_btn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.database);
            this.Controls.Add(this.pwrd);
            this.Controls.Add(this.uid);
            this.Controls.Add(this.server_name);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this._sever);
            this.Name = "SettingsForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Settings";
            ((System.ComponentModel.ISupportInitialize)(this.port_number)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Label _sever;
        private Label label2;
        private Label label3;
        private Label label1;
        private TextBox server_name;
        private TextBox uid;
        private TextBox pwrd;
        private TextBox database;
        private Button button1;
        private Button cancel_btn;
        private Label label5;
        private NumericUpDown port_number;
    }
}