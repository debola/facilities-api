﻿using System.ComponentModel.DataAnnotations;

namespace Monitor.Model
{
    //use jsonproperty attribute 
    public class TBIndicator
    {
        public int Id { get; set; }
        [Required] public string DatimCode { get; set; }
        [Required] public string ART_Number { get; set; }
        public string IP { get; set; }
        public string State { get; set; }
        public string Facility_Name { get; set; }
        public Gender Sex { get; set; }
        public string DOB { get; set; }
        public string Enrolment_Date { get; set; }
        public string Weight_At_Enrolment { get; set; }
        public string WHO_Stage_AtEnrolment { get; set; }
        public string Symptoms { get; set; }
        public string CD4_Count_At_HIV_Diagnosis { get; set; }
        public string Date_Of_First_CD4 { get; set; }
        public string TB_Status_At_Enrolment { get; set; }
        public string RegimenAtARTStart { get; set; }
        public string Prior_ART { get; set; }
        public string ART_Start_Date { get; set; }
        public string Current_ARV_Regimen { get; set; }
        public string Reasons_For_Stopping_ARVs { get; set; }
        public string Current_Weight { get; set; }
        public string Current_Weight_Date { get; set; }
        public string TPT_Start_Date { get; set; }
        public string TPT_Completion_Date { get; set; }
        public string Current_TB_Status { get; set; }
        public string CD4_Count_Current { get; set; }
        public string Date_Of_Current_CD4 { get; set; }
        public string CurrentViralLoad { get; set; }
        public string DateofCurrentViralLoad { get; set; }
        public string TB_Treatment_Completed { get; set; }
    }
}
