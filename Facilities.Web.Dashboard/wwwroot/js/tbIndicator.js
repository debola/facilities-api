﻿$(document).ready(function () {
	$('#tbIndicator').DataTable(
		{
			ajax: {
				url: `https://localhost:5001/api/Facilities/GetTbIndicators`,
				dataSrc: ''
			},
			columns: [
				{ data: 'ip' },
				{ data: 'state' },
				{ data: 'facility_Name' },
				{ data: 'datim_Code' },
				{ data: 'arT_Number' },
				{ data: 'sex' },
				{ data: 'dob' },
				{ data: 'enrolment_Date' },
				{ data: 'weight_At_Enrolment' },
				{ data: 'whO_Stage_AtEnrolment' },
				{ data: 'symptoms' },
				{ data: 'cD4_Count_At_HIV_Diagnosis' },
				{ data: 'date_Of_First_CD4' },
				{ data: 'tB_Status_At_Enrolment' },
				{ data: 'regimenAtARTStart' },
				{ data: 'prior_ART' },
				{ data: 'arT_Start_Date' },
				{ data: 'current_ARV_Regimen' },
				{ data: 'reasons_For_Stopping_ARVs' },
				{ data: 'current_Weight' },
				{ data: 'current_Weight_Date' },
				{ data: 'tpT_Start_Date' },
				{ data: 'tpT_Completion_Date' },
				{ data: 'current_TB_Staatus' },
				{ data: 'cD4_Count_Current' },
				{ data: 'date_Of_Current_CD4' },
				{ data: 'currentViralLoad' },
				{ data: 'dateofCurrentViralLoad' },
				{ data: 'tB_Treatment_Completed' },
			],
		}
	);
});