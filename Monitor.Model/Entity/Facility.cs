﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor.Model
{
    public class Facility
    {
        [Required] public int Id { get; set; }
        [Required]public int FormId { get; set; }
        [Required]public int PatientId { get; set; }
        [Required]public string DatimCode { get; set; }
        public string FacilityId { get; set; }
        public string FacilityName { get; set; }
        public string ImplementingPartner { get; set; }
        public string LGA { get; set; }
        public string State { get; set; }
        public string Username { get; set; } //data abstractor username
        public int Submissions { get; set; }
        public FormType FormType { get; set; }
        public string FormName { get; set; }
        public DateTime DateCreated { get; set; }
        public Gender Sex { get; set; }
        public byte CurrentAge { get; set; }
    }
}
