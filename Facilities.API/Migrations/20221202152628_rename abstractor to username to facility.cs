﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Facilities.API.Migrations
{
    public partial class renameabstractortousernametofacility : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "DataAbstractor",
                table: "Facility",
                newName: "Username");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Username",
                table: "Facility",
                newName: "DataAbstractor");
        }
    }
}
