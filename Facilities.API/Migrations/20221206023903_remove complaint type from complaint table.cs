﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Facilities.API.Migrations
{
    public partial class removecomplainttypefromcomplainttable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ComplaintType",
                table: "Complaints");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<byte>(
                name: "ComplaintType",
                table: "Complaints",
                type: "tinyint",
                nullable: false,
                defaultValue: (byte)0);
        }
    }
}
